###
import pynstestf as pf
import pyminit.tools as pt
import numpy as np
###
def checkProb(code, context):
    prob = pf.problem(code, context=context)
    x = 5* np.random.randn(prob.n)
    msg1 = 'problem code: {}\n Dimension: {}\n Initial point: {}'.format(code, prob.n, prob.x0 )
    msg2 = ' subgradient check: {}\n'.format(pt.checkNSBB(x, bb=prob.bb))
    return  '\n '.join([msg1, msg2])
###
codes = [('CB2', None), ('MAXQUAD', None), ('INSTABCP', {'n':50, 'lmda':1}), ('MAXQ', 10), ('MXHILB', 3), ('ChainedLQ', 10)]
codes = [('CB2', None), ('MAXQUAD', {'n': 5, 'np': 3}), ('INSTABCP', None), ('MAXQ', 10), ('MXHILB', 3), ('ChainedLQ', 10)]
# checkProb('MAXQ')
# checkProb('MXHILB')
###
for cc, context in codes:
    print(checkProb(cc, context))
###

