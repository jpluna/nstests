###
import pyminit.tools as pt
import numpy as np
import pynstestf as pf
###
def checkProb(code):
    prob = pf.problem(code)
    x = 5* np.random.randn(prob.n)
    msg1 = 'problem code: {}\n Dimension: {}\n Initial point: {}'.format(code, prob.n, prob.x0 )
    msg2 = ' subgradient check: {}\n'.format(pt.checkNSBB(x, bb=prob.bb))
    return  '\n '.join([msg1, msg2])
###
codes = pf.codes
# checkProb('MAXQ')
# checkProb('MXHILB')
for cc in codes:
    print(checkProb(cc))
###

