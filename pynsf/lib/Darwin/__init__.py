'''
pynsf
=========
This is a python interface to the C nstestf library. This library provides several black-box routines of nonsmooth functions.

'''

from .pynsf import *
