import numpy as np
import pynstestf as pf

code = 'MAXQ'
code = 'ChainedLQ'
context=50
print(pf.info(code))
problem = pf.problem(code, context=context)
print(problem)
x0 = problem.x0
bb = problem.bb
nIter = 10
step0 =1.0
print(x0)
for i in range(nIter):
    f, g, status=bb(x0, mode=2)
    # print(f, x0, g)
    print('[k={}] fval={}'.format(i,f))
    step = step0/(i+1)
    x0 = x0 - step * g
print(x0)
