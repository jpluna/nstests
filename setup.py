import setuptools
from distutils.core import setup
from distutils.extension import Extension
import platform
import os

import numpy

packageName = 'pynsf'
osy =  platform.system() 
packageDir = {oss: {'dir':'./{}/lib/{}'.format(packageName, oss), 'data':['{}.so'.format(packageName)]} for oss in ['Linux', 'Darwin']}

print('Operating system: {}\n'.format(osy))

setup( 
        name=packageName, 
        version='0.0.5.dev1', 
        description='Set of nonsmooth test functions.', 
        author='Juan Pablo Luna', 
        author_email='jpluna@yandex.com', 
        license='MIT',
        install_requires=[ 'numpy'], 
        packages=[packageName, ], 
        package_dir={packageName: packageDir[osy]['dir']}, 
        package_data = {packageName: packageDir[osy]['data'] }, 
        # include_package_data=True,
        # data_files=[('.',[os.path.join(libCASbbPath,'libCASbb.so')])],
        # data_files=[('/pynstestf',['pynstestf/C/extraLib/CASbb/lib/osx/libCASbb.so'])],
        # data_files=[('/usr/local/lib',['C/extraLib/CASbb/lib/osx/libCASbb.so'])],
        # eager_resources=['C/extraLib/CASbb/lib/osx/libCASbb.so'],
        # package_data={'pynstestf': ['libCASbb.so']},
        # packages=['pynstestf'],
        # package_dir={'pynstestf': '.'},
        # packages=['pynstestf'],
        # package_data={'pynstestf': ['C/extraLib/CASbb/lib/osx/libCASbb.so']},
        # include_package_data=True
        )
