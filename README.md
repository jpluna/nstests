# pynsf
This module provdies a **py**thon set of **n**on **s**mooth **f**untions for testing algorithms purpose.
This is a python interface to the C `nstestf` library. This library provides several black-box routines of nonsmooth functions.

## Install
via pip:

```python
pip install git+https://gitlab.com/jpluna/nstests.git
```
## Quick Start

Each nonsmooth function is idetified by a code (`str`). 
The list of all codes can be found via 
```python
In[1]: import pynsf
In[2]: pynsf.codes
Out[2]:
['CB2',
 'MAXQUAD',
 'INSTABCP',
 'MAXQ',
 'MXHILB',
 'ChainedLQ',
 'CAS_BADGUY',
 'CAS_NETWRK',
 'CAS_GOFFI0',
 'CAS_GOFFI1',
 'CAS_L1HILB',
 'CAS_HALFPS',
 'CAS_MAXQ',
 'CAS_MXANAL',
 'CAS_MXHILL',
 'CAS_MXQUAD',
 'CAS_NEMIRN',
 'CAS_POLAK1',
 'CAS_SHORMN',
 'CAS_TR48',
 'CAS_URASEV']

```

A `problem` object is available and accepts two arguments:
the `code` (mandatory) and the `context`( not mandatory, default value None).  The 
later may be and integer, float, string, dictionary, etc., according to the specific 
problem we want to create, and is used to tune its properties, when possible. If not provided, 
default configuration will be set. The description of each problem, and its specific 
`context` can be found in [documentation](./latex/man.pdf). The `problem` object has three
fields:

- n: the length of the decision variable.
- x0: a sugested initial point for starting an optimization method.
- bb: the black-box routine associated to the nonsmooth function. This black-black routine follows the pattern:
```
		bb(x, mode=0, contex=None)
			x: numpy array
			mode: int
		Returns
		-------
		fval: float
			the value of the function at point x
		sg: numpy array
			a subgradient of the function at point x
		status: int
			0 if the routine performed correctly. Otherwise, it is any value different to zero.
		Accodirng to the value of mode, the routine returns:
			fval, status (if mode = 0)
			g, status (if mode = 1)
			fval, g, status (if mode = 2)
```

```python
In[3]: problem = pynsf.problem('CB2')
In[4]: problem.n
Out[4]: 2
In[5]: x0 = problem.x0
In[6]: problem.bb(x0, mode=2)
Out[6]: (20.0, array([ 4., 32.]), 0)

In[7]: import numpy as np
In[8]: problem.bb(np.ones(problem.n),mode=0)
Out[8]: (2.0, 0)
```
### Using  the `context` argument

```python
In[9]: code = 'MAXQ'
In[10]: context = 5
In[11]: pynsf.info(code)
Out[9]: context=None(default) or int (dimenison).
In[13]: problem = pynsf.problem(code, context=context)
In[14] problem.n
Out[12] 5
In[13]: x0 = problem.x0
In[14]: x0
Out[14]: array([ 1.,  2., -3., -4., -5.])
In[15]: problem.bb(x0, mode=2)
Out[15]: (25.0, array([  0.,   0.,   0.,   0., -10.]), 0)
```
